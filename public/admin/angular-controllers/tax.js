
//==========================================================FAQ CONTROLLER=======================================================================================================
//====================================================================================================================================================================================
   app.controller('taxController', function($http, $scope , $window, $log, $q, $timeout, toastr) {
 
        var pro = this; 
		 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/tax',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.tax = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
		 /*=============================FUNCTION FOR UPDATE TAX=============================================================*/
		pro.updateTax = function(index,tax_id){ 
		     pro.tax_id = tax_id; 
			 pro.title = document.getElementsByName('title' + index + tax_id)[0].value;
			  pro.percentage = document.getElementsByName('percentage' + index + tax_id)[0].value;
			 
			pro.taxData = { "title": pro.title ,  "percentage": pro.percentage }; 
			var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/tax/'+pro.tax_id,
            data: pro.taxData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.data = data;
            $('#loading').css('display', 'none');  
		
            if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

            
			 location.reload();		
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		 toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		}
		 
   });