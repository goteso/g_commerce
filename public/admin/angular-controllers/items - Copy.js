//====================================================== ADD ITEM CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('itemAddController', function ($http, $scope, $window,toastr, $log, $q ) {
 
	     // get basic form to add product ============================================================== clean done
	     	var auth_user_id = document.getElementById('auth_user_id').value;
 
         $('#loading').css('display', 'block');
		  
		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/item?auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.addItem  = data;
			
			for(var i=0;i<$scope.addItem[0].fields.length;i++){
               if($scope.addItem[0].fields[i].identifier == 'item_categories'){ 
				 var categories =  $scope.addItem[0].fields[i].options ; 
				 $scope.categories = categories; 
			 } 
			}
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
        });  


		
		
		 $scope.searchData = function (query) {
		    var search_term = query.toUpperCase(); 
		    $scope.people = [];
		    angular.forEach($scope.categories, function (item) {
			    if (item.label.toUpperCase().indexOf(search_term) >= 0)
				    $scope.people.push(item);
		        });
				console.log(JSON.stringify($scope.people));
           return $scope.people;
		   
	    };
		
		
		$scope.getCatValue = function(value){  
			$scope.category_ids = [];  
			$scope.category_ids.join();
			console.log($scope.category_ids.join());
			for(var i=0;i<=value.length; i++){
			$scope.category_ids.push(value[i].value); 
			} 
			 
			
		}
		
       /*$scope.searchData = function(query) {
		   // alert(JSON.stringify($scope.categories));
		   return $scope.categories;
		    
           /* return $http.get(APP_URL + "/api/v1/category?search_text=" + query , {
                params: {
                    q: query
                }
            })
			  .then(function(response) { 	 			  
                return response.data.data.data;
            }) ;*
			
			//tags.query().$promise;
       };*/
		
		
		$scope.querySearch = function (query) { 
			return $http.get(APP_URL + "/api/v1/category?search_text=" + query , {
                params: {
                    q: query
                }
            })
			  .then(function(response) { 	 			  
                return response.data.data.data;
            }) ;
			  
    }
	
		
	 
	 
	  //========= FUNCTIONS FOR GET VENDOR FOR STORE A AREA====================================================================================== 
		  $scope.vendorSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/users?user_type=3&search_text="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedVendorChange = function(values, data) { 
                $log.info('Item changed to ' + JSON.stringify(values));
				$scope.vendor_id = values.user_id;  
            }
			
			
			//========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search_text="+query, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedStoreChange = function(values, data) {
                $log.info('Item changed to ' + JSON.stringify(values)); 
                $scope.store_id =  values.store_id  ;  
            
            }
			
	
// FUNCTION FOR ADD ITEM  ==============================================================  
	     
		
           $scope.storeItem = function(){ 
	 

	  var item_photo = $('#item_photo').val();
 
 for(var i=0;i<$scope.addItem[0].fields.length;i++){
 if($scope.addItem[0].fields[i].identifier == 'item_photo'){
	 $scope.addItem[0].fields[i].value = item_photo;
 } 
 
 if($scope.addItem[0].fields[i].type == 'api'){
 if($scope.addItem[0].fields[i].identifier == 'vendor_id'){
	 $scope.addItem[0].fields[i].value = $scope.vendor_id;
 } 
 if($scope.addItem[0].fields[i].identifier == 'store_id'){
	 $scope.addItem[0].fields[i].value = $scope.store_id;
 } 
 }
 if($scope.addItem[0].fields[i].identifier == 'item_categories'){
	 $scope.addItem[0].fields[i].value = $scope.category_ids;
 } 
}
           	console.log(JSON.stringify($scope.addItem));
 

			    var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/form/item',
            data:  $scope.addItem,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data;
			
			if(data.status_code == 1){
			toastr.success(data.message, 'Success');
            $('#loading').css('display', 'none');   
            	console.log(JSON.stringify(data)); 
			 
			$scope.item_id = data.data.id
			  $window.location.href = 'item/'+$scope.item_id;
			}
			else{toastr.error(data.message, 'Error');}
        })
		.error(function (data, status, headers, config) {  	
		
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 
		   };			   
		  
	});





	
	
	
	
//======================================================VIEW ITEMS LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('itemsController', function ($http,$scope,$window,toastr,$location) {
		var auth_user_id = document.getElementById('auth_user_id').value;
        var pro = this; 
        $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/items?auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.products = data;
			
			
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	
	
	
	// FUNCTION FOR DELETE ITEM  ============================================================== clean done
	pro.deleteItem = function(itemId) {
		    
			$scope.item_id = itemId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/items/'+$scope.item_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
			        toastr.success(data.message,'Success!');
                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
	
 	});
	
	
	
	
	
	

	
//=========================================================EDIT ITEM CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('editItemsController', function ($http, $scope, $window,toastr,$log, $q) {
 
  $scope.item_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1); 


	 
	        $('#loading').css('display', 'block'); 
		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/item/'+$scope.item_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.editItem  = data;
			console.log(JSON.stringify(data));
			 $('#loading').css('display', 'none');  
	 for(var j=0;j<=$scope.editItem[0].fields.length;j++){  
          if($scope.editItem[0].fields[j].identifier == 'vendor_id'){ 
	 var vendor =  $scope.editItem[0].fields[j].value;
   $scope.vendor = vendor;	 
     } 
 else if($scope.editItem[0].fields[j].identifier == 'store_id'){
	 var store = $scope.editItem[0].fields[j].value  ;
	  $scope.store = store;	 
 }   
             if($scope.editItem[0].fields[j].identifier == 'item_photo'){ 
				 var item_data_image =  $scope.editItem[0].fields[j].value ; 			 
				 document.getElementById('item_data').value = item_data_image; 
			 } 
			 if($scope.editItem[0].fields[j].identifier == 'item_categories'){ 
				 var categories =  $scope.editItem[0].fields[j].options ; 
				 $scope.categories = categories; 
			 } 
			}
			 
			  
           
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });  


		 
		 

		
			 $scope.searchCatData = function (query) {
		    var search_term = query.toUpperCase(); 
		    $scope.cat = [];
		    angular.forEach($scope.categories, function (item) {
			    if (item.label.toUpperCase().indexOf(search_term) >= 0)
				    $scope.cat.push(item);
		        });
				console.log(JSON.stringify($scope.cat));
           return $scope.cat;
		   
	    };
		
		
		$scope.getCatEditValue = function(value){  
			$scope.category_ids = [];  
			$scope.category_ids.join();
			console.log($scope.category_ids.join());
			for(var i=0;i<=value.length; i++){
			$scope.category_ids.push(value[i].value); 
			} 
			 
			
		}
		
	 	
		
	  //========= FUNCTIONS FOR GET VENDOR FOR STORE A AREA====================================================================================== 
		  $scope.vendorSearch = function(query){ 
		        return $http.get(APP_URL+"/api/v1/users?user_type=3&search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedVendorChange = function(values, data) { 
                $log.info('Item changed to ' + JSON.stringify(values));
				 $scope.vendor_id =  values.user_id ;   
			    if($scope.vendor_id != undefined || $scope.vendor_id != null){
				   $scope.vendor = $scope.vendor_id;
			    }
            }
			
			
			//========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search="+query, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedStoreChange = function(values, data) {
                $log.info('Item changed to ' + JSON.stringify(values)); 
                 $scope.store_id =  values.store_id ;   
			    if($scope.store_id != undefined || $scope.store_id != null){
				   $scope.store = $scope.store_id;
			    }  
            
            }
	
	
	//FUNCTION FOR UPDATE ITEM==============================================================
		   $scope.updateItem = function(){ 
		   		  var item_photo = $('#item_photo').val();
 
 for(var i=0;i<$scope.editItem[0].fields.length;i++){
  if($scope.editItem[0].fields[i].identifier == 'item_photo'){
	 $scope.editItem[0].fields[i].value = item_photo;
	  // document.getElementById('preview_image').src = APP_URL+'/images/items/'+item_photo;
	   //console.log("this is deepu path - "+APP_URL+'/images/items/' + $('#item_photo').val(data));
 }  
 
 if($scope.editItem[0].fields[i].type == 'api'){
 if($scope.editItem[0].fields[i].identifier == 'vendor_id'){
	 $scope.editItem[0].fields[i].value = $scope.vendor;
 } 
 if($scope.editItem[0].fields[i].identifier == 'store_id'){
	 $scope.editItem[0].fields[i].value = $scope.store;
 } 
 }
 
 if($scope.editItem[0].fields[i].identifier == 'item_categories'){
	 $scope.editItem[0].fields[i].value = $scope.category_ids;
 } 
}
     
	 console.log(JSON.stringify($scope.editItem));
	 
			    var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/form/item/'+$scope.item_id,
            data:  $scope.editItem,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data; 
             toastr.success(data.message, 'Success');
            $('#loading').css('display', 'none');    
			 document.getElementById("res").value = JSON.stringify(data); 
        })
		.error(function (data, status, headers, config) {  	
		
             toastr.error(data.message, 'Error');
		      document.getElementById("res").value = JSON.stringify(data);
        });  
 
		   };		

$scope.selected = 0;









	

 //  ADD UPDATE VARIANTS (for Add Product Basic Form Autocomplete ) =====================================================================
	$scope.add = function (index) { 
		var value = {};
		$scope.editItem[2].fields[index].values.push(value); 
	}
 
	$scope.save_variant = function (index, tab_id) {
		
        $scope.variant_id = document.getElementsByName('variant_id' + index + tab_id)[0].value;
		$scope.item_variant_price_difference = document.getElementsByName('price_difference' + index + tab_id)[0].value;
        $scope.item_variant_stock_count = document.getElementsByName('stock_count' + index + tab_id)[0].value;
        $scope.item_variant_price = document.getElementsByName('price' + index + tab_id)[0].value;
        $scope.item_variant_type_id = document.getElementsByName('product_variant_type_id' + index + tab_id)[0].value;
        $scope.item_id = $scope.item_id;
		$scope.item_variant_value_title = document.getElementsByName('title' + index + tab_id)[0].value;
        $scope.item_variant_photo = '';

		$scope.variant_data = {
			"item_variant_type_id": $scope.item_variant_type_id,
			"item_variant_value_title": $scope.item_variant_value_title,
			"item_id": $scope.item_id,
			"item_variant_price" : $scope.item_variant_price,
			"item_variant_price_difference": $scope.item_variant_price_difference,
			"item_variant_stock_count": $scope.item_variant_stock_count, 
			"item_variant_photo": $scope.item_variant_photo
		};
         console.log(JSON.stringify($scope.variant_data));
		
		
		if($scope.variant_id == ''){  
		var request = $http({
			method: "POST",
			url: APP_URL+'/api/v1/items-variants',
			data: $scope.variant_data,
			headers: {
				'Accept': 'application/json'
			}
		}); 
		request.success(function (data) {
			toastr.success(data.message,'Success!');		
	         $('input[name="' + 'variant_id' + index + tab_id + '"]').val(data.data.id) ;
	 
        }).error(function (data, status, headers, config) { 
		toastr.error('Error Occurs','Error!');
	   }); 
	   
		}
		
		else{
			var request = $http({
			method: "PUT",
			url: APP_URL+'/api/v1/items-variants/'+$scope.variant_id,
			data: $scope.variant_data,
			headers: {
				'Accept': 'application/json'
			}
		}); 
		request.success(function (data) {
			toastr.success(data.message,'Success!');		
	         $('input[name="' + 'id' + index + tab_id + '"]').val(data.id) ;
	 
        }).error(function (data, status, headers, config) { 
		toastr.error('Error Occurs','Error!');
	   }); 
	   
		}
		}
		
		
		
		// Delete Product Variant (for Add Product Basic Form Autocomplete ) ================================================================
	$scope.delete_variant = function (index, tab_id ,index1) { 
	
		$scope.editItem[2].fields[index1].values.splice(index, 1);

		$scope.variant_id = document.getElementsByName('variant_id' + index + tab_id)[0].value;
		 
		  if (confirm("Are you sure?")) {
		$http.get(APP_URL+'/api/v1/items-variants/'+$scope.variant_id)
			.success(function (data, status, headers, config) {
				toastr.success(data.message,'Success!');
			})
			var request = $http({
			method: "DELETE",
			url: APP_URL+'/api/v1/items-variants/'+$scope.variant_id,
			data: '',
			headers: {
				'Accept': 'application/json'
			}
		}); 
		request.success(function (data) {
			$scope.data = data;
			toastr.success(data.message,'Success!');		 
	 
        }).error(function (data, status, headers, config) {
				 toastr.error('Error Occurs','Error!'); 
			});
		  }
   };
 		   
	});



