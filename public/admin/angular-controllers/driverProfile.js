 app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/driver-basicInfo.html",
        controller : "profileController"
    })
    .when("/tasks", {
        templateUrl : APP_URL+"/driver-task.html",
        controller : "tasksController"
    });
});
 
 
app.controller('profileController', function($http,$scope,toastr) {
 
   $scope.url = window.location.href;

 // Jugaadi code starts here ====================created by Deepakshi Singla
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 $scope.driver_id = str.replace("#", "");
  // Jugaadi code starts here ====================created by deepakshi Singla ends

 
     
	   $('#loading').css('display', 'none');  
	   
		 var request = $http({
                 method: "GET",
                 url: APP_URL+'/api/v1/users/'+$scope.driver_id+'?include_meta=true',
                 data:  '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {  
			  $scope.profileData = data;
			  $('#loading').css('display', 'none');
			 document.getElementById("res").value =JSON.stringify(data); 
         }).error(function (data, status, headers, config) { 
			  
			document.getElementById("res").value =JSON.stringify(data);
               
        });   
});




 


app.controller('tasksController', function($http,$scope,toastr) {
   $scope.url = window.location.href;

 // Jugaadi code starts here ====================created by Deepakshi Singla
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 $scope.driver_id = str.replace("#", "");
  // Jugaadi code starts here ====================created by deepakshi Singla ends

 
     
 
	 var request = $http({
                 method: "GET",
                 url: APP_URL+'/api/v1/task?driver_id='+$scope.driver_id,
                 data:  '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {  
			  $scope.taskData = data;
			  $('#loading').css('display', 'none');
			 document.getElementById("res").value =JSON.stringify(data); 
         }).error(function (data, status, headers, config) { 
			  
			document.getElementById("res").value =JSON.stringify(data);
               
        });   
	
});





