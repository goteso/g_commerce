 app.directive('hcChart', function() {
     return {
         restrict: 'E',
         template: '<div id="container" style="margin: 0 auto">not working</div>',
         scope: {
             options: '='
         },
         link: function(scope, element) {
             var chart = new Highcharts.chart(element[0], scope.options);
             $(window).resize(function() {
                 chart.reflow();
             });
         }
     };
 })

 
 app.factory('Excel',function($window){
        var uri='data:application/vnd.ms-excel;base64,',
            template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
            format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
        return {
            tableToExcel:function(tableId,worksheetName){
                var table=$(tableId),
                    ctx={worksheet:worksheetName,table:table.html()},
                    href=uri+base64(format(template,ctx));
                return href;
            }
        };
    })
	

 app.factory('dataShare', function($rootScope) {
     var service = {};
     service.data = false;
     service.sendData = function(data) {
		 alert(JSON.stringify(data));
         this.data = data;
         $rootScope.$broadcast('data_shared');
		 
     };
     service.getData = function() {
		 alert(JSON.stringify(this.data));
         return this.data;
     };
     return service;
 });


 
 //====================================================== REPORTS CONTROLLER=================================================================================
//==============================================================================================================================================================
 
 
 app.controller('reportsViewController', function($http, $scope, $window, $filter, dataShare) {

  $('#loading').css('display', 'block');
  
     var request = $http({
         method: "GET",
         url: APP_URL+'/api/v1/reports',
         data: '',
         headers: {
             'Accept': 'application/json'
         }
     });

     /* Check whether the HTTP Request is successful or not. */
     request.success(function(data) { 
         $scope.reports = data; 
     $('#loading').css('display', 'none');
         //$scope.product_id = data["user_data"][0]["product_id"]; 
         //$window.location.href = 'product_edit/'+$scope.product_id;
     });






     $scope.revenueReport = {
         title: {
             text: ''
         },
         xAxis: {
             categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
             labels: {
                 rotation: -45,
                 style: {
                     fontSize: '11px',
                     fontFamily: 'Open Sans, sans-serif'
                 }
             }
         },
         yAxis: [{ // Primary yAxis
             labels: {
                 format: ' {value}',
                 style: {
                     color: Highcharts.getOptions().colors[0]
                 }
             },
             title: {
                 text: '',
                 style: {
                     color: Highcharts.getOptions().colors[0]
                 }
             }
         }],
         plotOptions: {
             series: {
                 label: {
                     connectorAllowed: false
                 }
             }
         },

         series: {
             showInNavigator: true
         },
         credits: {
             enabled: false
         },
         legend: {
             enabled: true,
             align: 'right',
             verticalAlign: 'top',
             layout: 'vertical',
             x: 0,
             y: 100
         },
         exporting: {
             enabled: false
         },

         // legend: { },
         options: {
             chart: {
                 color: Highcharts.getOptions().colors[0]
             }
         },
         rangeSelector: {
             floating: true,
             y: -65,
             verticalAlign: 'bottom'
         },

         navigator: {
             margin: 60
         },

         series: [{
                 name: 'Revenue',
                 type: 'spline',
                 data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
             }

             /**, 
					{ name:'Users Joined',
						type:'spline',
                        data: $scope.revenueReport[0].users_y_array
					},{ name:'Revenues',
						type:'spline',
                        data: $scope.revenueReport[0].revenues_y_array
					}**/

         ],

         colors: ['#4583EB', '#44a5f4', '#FF8080'],

     };




     $scope.orderReport = {
         title: {
             text: ''
         },
         xAxis: {
             categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
             labels: {
                 rotation: -45,
                 style: {
                     fontSize: '11px',
                     fontFamily: 'Open Sans, sans-serif'
                 }
             }
         },
         yAxis: [{ // Primary yAxis
             labels: {
                 format: '{value}',
                 style: {
                     color: Highcharts.getOptions().colors[0]
                 }
             },
             title: {
                 text: '',
                 style: {
                     color: Highcharts.getOptions().colors[0]
                 }
             }
         }],
         plotOptions: {
             series: {
                 label: {
                     connectorAllowed: false
                 }
             }
         },

         series: {
             showInNavigator: true
         },
         credits: {
             enabled: false
         },
         legend: {
             enabled: true,
             align: 'right',
             verticalAlign: 'top',
             layout: 'vertical',
             x: 0,
             y: 100
         },
         exporting: {
             enabled: false
         },

         // legend: { },
         options: {
             chart: {
                 color: Highcharts.getOptions().colors[0]
             }
         },
         rangeSelector: {
             floating: true,
             y: -65,
             verticalAlign: 'bottom'
         },

         navigator: {
             margin: 60
         },

         series: [{
                 name: 'Orders',
                 type: 'spline',
                 data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
             }

             /**, 
					{ name:'Users Joined',
						type:'spline',
                        data: $scope.revenueReport[0].users_y_array
					},{ name:'Revenues',
						type:'spline',
                        data: $scope.revenueReport[0].revenues_y_array
					}**/

         ],

         colors: ['#44a4f3', '#44a5f4', '#FF8080'],

     };




     $scope.userReport = {
         title: {
             text: ''
         },
         xAxis: {
             categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
             labels: {
                 rotation: -45,
                 style: {
                     fontSize: '11px',
                     fontFamily: 'Open Sans, sans-serif'
                 }
             }
         },
         yAxis: [{ // Primary yAxis
             labels: {
                 format: ' {value}',
                 style: {
                     color: Highcharts.getOptions().colors[1]
                 }
             },
             title: {
                 text: '',
                 style: {
                     color: Highcharts.getOptions().colors[1]
                 }
             }
         }],
         plotOptions: {
             series: {
                 label: {
                     connectorAllowed: false
                 }
             }
         },

         series: {
             showInNavigator: true
         },
         credits: {
             enabled: false
         },
         legend: {
             enabled: true,
             align: 'right',
             verticalAlign: 'top',
             layout: 'vertical',
             x: 0,
             y: 100
         },
         exporting: {
             enabled: false
         },

         // legend: { },
         options: {
             chart: {
                 color: Highcharts.getOptions().colors[1]
             }
         },
         rangeSelector: {
             floating: true,
             y: -65,
             verticalAlign: 'bottom'
         },

         navigator: {
             margin: 60
         },

         series: [{
                 name: 'Users',
                 type: 'spline',
                 data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 95.6, 54.4]
             }

             /**, 
					{ name:'Users Joined',
						type:'spline',
                        data: $scope.revenueReport[0].users_y_array
					},{ name:'Revenues',
						type:'spline',
                        data: $scope.revenueReport[0].revenues_y_array
					}**/

         ],

         colors: ['#4583EB', '#44a5f4', '#FF8080'],

     };




	 
	 
     var curr = new Date; // get current date 
     var day = curr.getDay();
     //var first = curr.getDate(); // First day is the day of the month - the day of the week
     //var last = first + 6; // last day is the first day + 6
     var first = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() + (day == 0 ? -6 : 1) - day);
     var last = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() + (day == 0 ? -6 : 1) - day + 6);
     $scope.firstday = $filter('date')(first, "yyyy-MM-dd");
     $scope.lastday = $filter('date')(last, "yyyy-MM-dd");



     $scope.getReport = function(api) {
         alert(APP_URL+'/'+api+'&date_from='+$scope.firstday+'&date_to='+$scope.lastday);
         var request = $http({
             method: "GET",
             url: APP_URL+'/'+api+'&date_from='+$scope.firstday+'&date_to='+$scope.lastday,
             data: '',
             headers: {
                 'Accept': 'application/json'
             }
         });

         /* Check whether the HTTP Request is successful or not. */
         request.success(function(data) {
             $scope.reportsData = data; 
             dataShare.sendData($scope.reportsData);
			 alert($scope.reportsData);
             $window.location.href = 'reports-detail';

             console.log(JSON.stringify($scope.reportsData));
             $('#loading').css('display', 'none');
         });




     }



 });


//====================================================== REPORT VIEW LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 

 app.controller('reportController', function($http, $scope, Excel, $timeout, $location, $filter, dataShare) {
    $('#loading').css('display', 'none');
 
	  alert(dataShare.getData());
	  $scope.$on('data_shared', function() {
		    alert(dataShare.getData());
		  alert('1');
         var text = dataShare.getData();
         $scope.reportsData = text;

     });
	 
/*
     var url = $location.absUrl();

     var n = url.lastIndexOf('/');
     var result = url.substring(n + 1);

*/


     //$scope.firstday = '';
     //$scope.lastday = '';

     var curr = new Date; // get current date 
     var day = curr.getDay();
     //var first = curr.getDate(); // First day is the day of the month - the day of the week
     //var last = first + 6; // last day is the first day + 6
     var first = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() + (day == 0 ? -6 : 1) - day);
     var last = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() + (day == 0 ? -6 : 1) - day + 6);
     $scope.firstday = $filter('date')(first, "yyyy-MM-dd");
     $scope.lastday = $filter('date')(last, "yyyy-MM-dd");



     $scope.thisWeek = function() {
         var curr = new Date; // get current date 
         var day = curr.getDay();
         //var first = curr.getDate(); // First day is the day of the month - the day of the week
         //var last = first + 6; // last day is the first day + 6
         var first = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() + (day == 0 ? -6 : 1) - day);
         var last = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() + (day == 0 ? -6 : 1) - day + 6);
         $scope.firstday = $filter('date')(first, "yyyy-MM-dd");
         $scope.lastday = $filter('date')(last, "yyyy-MM-dd");
     }

     $scope.lastWeek = function() {
         var curr = new Date; // get current date 
         var day = curr.getDay();
         var first = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() + (day == 0 ? -6 : 1) - day - 7);
         var last = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() + (day == 0 ? -6 : 1) - day - 1);
         $scope.firstday = $filter('date')(first, "yyyy-MM-dd");
         $scope.lastday = $filter('date')(last, "yyyy-MM-dd");

     }

     $scope.thisMonth = function() {
         var curr = new Date; // get current date 
         var first = new Date(curr.getFullYear(), curr.getMonth(), 1);
         var last = new Date(curr.getFullYear(), curr.getMonth() + 1, 0);
         $scope.firstday = $filter('date')(first, "yyyy-MM-dd");
         $scope.lastday = $filter('date')(last, "yyyy-MM-dd");
     }

     $scope.lastMonth = function() {
         var curr = new Date; // get current date 
         var first = new Date(curr.getFullYear(), curr.getMonth() - 1, 1);
         var last = new Date(curr.getFullYear(), curr.getMonth(), 0);
         $scope.firstday = $filter('date')(first, "yyyy-MM-dd");
         $scope.lastday = $filter('date')(last, "yyyy-MM-dd");
     }

     $scope.thisQuarter = function() {
         var curr = new Date();
         var quarter = Math.floor((curr.getMonth() / 3));
         var first = new Date(curr.getFullYear(), quarter * 3, 1);
         var last = new Date(first.getFullYear(), first.getMonth() + 3, 0);
         $scope.firstday = $filter('date')(first, "yyyy-MM-dd");
         $scope.lastday = $filter('date')(last, "yyyy-MM-dd");
     }

     $scope.lastQuarter = function() {
         var curr = new Date();
         var quarter = Math.floor((curr.getMonth() / 3));
         var first = new Date(curr.getFullYear(), quarter * 3 - 3, 1);
         var last = new Date(first.getFullYear(), first.getMonth() + 3, 0);
         $scope.firstday = $filter('date')(first, "yyyy-MM-dd");
         $scope.lastday = $filter('date')(last, "yyyy-MM-dd");
     }

     //$scope.user_id = $('#userId').val();
    


     /*   var request = $http({
                 method: "GET",
                 url: APP_URL+'/api/v1/'+result+'&table_fields=order_id,order_status,total,customer_name,store_title,created_at_formatted&date_from='+$scope.firstday+'&date_to='+$scope.lastday+'&type=report',
                 data: '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. *
            request.success(function (data) { 
			console.log(JSON.stringify(data));
			 	 $scope.reportsData =  data; 
               $('#loading').css('display', 'none');
            });  
			*/


     $scope.IsVisible = true;
     $scope.IsHidden = false;
     $scope.ShowHide = function() {
         //If DIV is visible it will be hidden and vice versa.
         $scope.IsHidden = true;
         $scope.IsVisible = false;
     };




     $scope.generateReport = function() {

         var request = $http({
             method: "GET",
             url: APP_URL + '/api/v1/' + result + '?&table_fields=order_id,order_status,total,customer_name,store_title,created_at_formatted&date_from=' + $scope.firstday + '&date_to=' + $scope.lastday + '&type=report',
             data: '',
             headers: {
                 'Accept': 'application/json'
             }
         });

         /* Check whether the HTTP Request is successful or not. */
         request.success(function(data) {
             $scope.reportsData = data;
             console.log(JSON.stringify($scope.reportsData));
             $('#loading').css('display', 'none');
         });


     }

     /**Get notes values from json**/
     angular.forEach($scope.reportsData, function(data) {
         if (data.notes == '') {
             $scope.notes = 'There are no any notes';
         } else {
             $scope.notes = data.notes;
         }
     });

     /** Submit notes form data **/
     $scope.submit = function() {
         if (this.notes) {
             $scope.notes = this.notes;
             $scope.IsHidden = false;
             $scope.IsVisible = true;
             //$scope.notes = '';
         }
         if (this.notes == '') {
             $scope.notes = 'There are no any notes';
             $scope.IsHidden = false;
             $scope.IsVisible = true;
         }

     };



     $scope.exportToExcel = function(tableId) { // ex: '#my-table'
         var exportHref = Excel.tableToExcel(tableId, 'WireWorkbenchDataExport');
         $timeout(function() {
             location.href = exportHref;
         }, 100); // trigger download
     }

     /*  $scope.refresh = function(){
         $http.get('/api/users')
               .success(function(data){
                    $scope.users = data;
               });
     } */




     $scope.printToCart = function(tableToExport) {
         var innerContents = document.getElementById('tableToExport').innerHTML;
         var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
         popupWinindow.document.open();
         popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
         popupWinindow.document.close();
     }



     $scope.export = function() {
         html2canvas(document.getElementById('exportthis'), {
             onrendered: function(canvas) {
                 var data2 = canvas.toDataURL();
                 var docDefinition = {
                     content: [{
                         image: data2,
                         width: 500,
                     }]
                 };
                 pdfMake.createPdf(docDefinition).download("test.pdf");
             }
         });
     };

 });


 
 
 //====================================================== REPORTS DATA CONTROLLER=================================================================================
//==============================================================================================================================================================
 

 app.controller('reportDataController', function($http, $scope, $window) {

     $scope.determinateValue = 0;
     $scope.isLoading = true;
     $scope.isDisabled = false;
     var request = $http({
         method: "GET",
         url: APP_URL + '/api/v1/reports',
         data: '',
         headers: {
             'Accept': 'application/json'
         }
     });

     /* Check whether the HTTP Request is successful or not. */
     request.success(function(data) {

         $scope.determinateValue = 50;

         setTimeout(function() {
             $scope.isLoading = false;
             $scope.isDisabled = true;
             //alert($scope.isLoading);
         }, 20);

         $scope.determinateValue = 100;
         $scope.isDisabled = true;


         $scope.reports = data;

         //$scope.product_id = data["user_data"][0]["product_id"];

         //$window.location.href = 'product_edit/'+$scope.product_id;
     });




 });