  app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/approved-requests.html",
        controller : "approvedRequestController"
    })
    .when("/pending-requests", {
        templateUrl : APP_URL+"/pending-requests.html",
        controller : "pendingRequestController"
    });
});
  
  
  app.controller('approvedRequestController', function($http,$scope,toastr,$log,$location) {
	  
	  var auth_user_id = document.getElementById('auth_user_id').value;
  var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores/log-update-request/list?auth_user_id='+auth_user_id+'&approved_status=1',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.approvedRequestData  = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  

		
		
		$scope.ShowData = function(data){  
	 $scope.showRequestDetail = data;
	 $('#showRequestModal').modal('show');
	 console.log(JSON.stringify(data));
	  
 }
 
 
  });
  
app.controller('pendingRequestController', function($http,$scope,toastr,$log,$location) {
	
 	var auth_user_id = document.getElementById('auth_user_id').value;
  var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores/log-update-request/list?auth_user_id='+auth_user_id+'&approved_status=0',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.pendingRequestData  = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  


		
		 
 $scope.ShowData = function(data){  
	 $scope.showRequestDetail = data;
	 $('#showRequestModal').modal('show');
	 console.log(JSON.stringify(data));
	  
 }
 
 
 
  $scope.approveRequest = function(value){
	  
	 $scope.request_id = value.id;
	 $scope.store_title = value.request_data_object.store_title;
	 $scope.store_photo = value.request_data_object.store_photo;
	 $scope.latitude = value.request_data_object.latitude;
	 $scope.longitude = value.request_data_object.longitude;
	  $scope.requestDataDetail = 
	  {
    "store_title":$scope.store_title,
    "store_photo":$scope.store_photo,
    "latitude": $scope.latitude,
    "longitude": $scope.longitude 
       };

	  var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/stores/approve-log-update-request/'+$scope.request_id+'?auth_user_id='+auth_user_id,
            data: $scope.requestDataDetail ,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data  = data;
           if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
					location.reload();
					}
					else{toastr.error(data.message,'Error!');}
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  
		
 }
 
 
 
 $scope.rejectRequest = function(value){
	  
	 $scope.request_id = value.id;
	 $scope.store_title = value.store_details[0].store_title;
	 $scope.store_photo = value.store_details[0].store_photo;
	 $scope.latitude = value.store_details[0].latitude;
	 $scope.longitude = value.store_details[0].longitude;
	  $scope.requestDataDetail1 = 
	  {
    "store_title":$scope.store_title,
    "store_photo":$scope.store_photo,
    "latitude": $scope.latitude,
    "longitude": $scope.longitude 
       };

	  var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/stores/reject-log-update-request/'+$scope.request_id+'?auth_user_id='+auth_user_id,
            data: $scope.requestDataDetail1 ,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data  = data;
           if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
					location.reload();
					}
					else{toastr.error(data.message,'Error!');}
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  
		
 }
 
});


 