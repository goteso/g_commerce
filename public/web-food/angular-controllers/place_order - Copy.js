  
   app.controller('checkoutController',[ '$scope','cartStorage', function($http,$scope,$window,$log, toastr, cartStorage,$location) {
	    /*var _this = this;
	   _this.cartStorage = cartStorage.cart;
	  console.log(JSON.stringify(_this.cartStorage));
	  return _this.cartStorage; 
	  */
	  
	  $scope.items = cartStorage.getItems();   
	 console.log(JSON.stringify($scope.items));
	  
  }]);
  
 


//==========================================================ADD ORDER CONTROLLER====================================================================================================
//==================================================================================================================================================================================
    app.controller('orderController', ['$scope','cartStorage', function($http,$scope,$window,$log, toastr, cartStorage,$location) {
     
	
        var _this = this;
       
	   
	     _this.items = cartStorage.getItems();   
	 console.log(JSON.stringify(_this.items));
	 
	 
	    _this.cartStorage = cartStorage.cart;
        _this.total = 0;

        $('.order-content').css('display', 'none');
        $('#loading').css('display', 'block'); 
        
		//========= ONLOAD GET ADD ORDER FORM  VIA CALLING API==================================================================
		
		 var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/orders/add/form/',
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
           //console.log(JSON.stringify(data)); 
			 _this.addOrder = data.categories_products;
            _this.addOrderMeta = data.order_meta_fields;
            _this.UserListData = data.order_meta_fields.fields.fields;
            $('#loading').css('display', 'none');
            $('.order-content').css('display', 'block');
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
		
		
		 
	  _this.orderCalculate = function() { 
			  $scope.order_items = _this.cartStorage.items;
			$scope.order_items.data = {"coupon_code":"Testd1",  "store_id":"11", "loyalty_points":"","customer_id":"266","items": $scope.order_items};
				 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_items.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
                $(".cart").hide();
                $(".main-cart").show();					 
				console.log(JSON.stringify(_this.paymentData));
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
			
		 
		}; 
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

	//========= FUNCTIONS FOR ADD PRODUCTS TO CART ====================================================================================
	//=================================================================================================================================
	
	
        _this.addToCart = function(item) {
            $scope.item = item;
            $scope.id = item.item_id;
            $scope.item_id = {"item_id": $scope.id }
			
			//CHECK VARIANTS EXIST OR NOT=====================================================================
			
			//IF DOES NOT EXIT-----------------------------------------------------------------
            if (item.variant_exist == '0') {
                var alreadyInCart = '';
                var cartIndex = 0;

                if (_this.cartStorage.items.length == 0) {
                    item.discounted_price = item.item_price;
                    item.quantity = 1;
				    item.variants = [];
                    _this.cartStorage.items.push(item); 
				 _this.orderCalculate();
                    return;
				}

                for (var i = 0; i < _this.cartStorage.items.length; i++) {
                    if (_this.cartStorage.items[i].item_id == item.item_id) {
                        alreadyInCart = 'YES';
                        cartIndex = i;
                    }
                }

				
                if (alreadyInCart == 'YES') {
                    _this.cartStorage.items[cartIndex].quantity = _this.cartStorage.items[cartIndex].quantity + 1;
				 _this.orderCalculate();
                    return;
                } 
				
				else {
                    item.quantity = 1;
				    item.variants = [];
                    item.discounted_price = item.item_price;
                    _this.cartStorage.items.push(item); 
					
					 _this.orderCalculate();
                    document.getElementById("res").value = JSON.stringify(_this.cartStorage.items);
				
				   
                }
            }
 
			//IF VARIANTS EXIT------------------------------------------------------------------
			else { 
				//CALL API FOR GET PRODUCT VARIANTS===========================
				var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/items-variants?orderby=item_variant_value_title&item='+$scope.id,
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			 _this.variantsdata = data.data;
            document.getElementById('res').value = JSON.stringify(data); 
            $('#loading').css('display', 'none');
			 $("#variantsModal").modal('show');
                     $scope.variantsArray = [];
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
			
				 
            }

        };

		
		  $scope.variantsSelectedTitle = [];
		
		
		//FUNCTION FOR SHOW PRODUCT VARIANTS AS CHECKBOXES=======================================================================
        $scope.getVariantValue = function(variants) {
			alert(JSON.stringify(variants));
            $scope.variantsSelectedTitle = []; 
            var alreadyInVariants = '';
            var variantsIndex = 0;
            $scope.variant = variants;
            if ($scope.variantsArray.length == 0) {
				alert('1');
				
                $scope.variantsArray.push(variants);
				alert(JSON.stringify($scope.variantsArray))
                return;
            }

            for (var i = 0; i < $scope.variantsArray.length; i++) {
                if ($scope.variantsArray[i].item_variant_value_id == variants.item_variant_value_id) {
					alert('2');
					alert(JSON.stringify($scope.variantsArray))
                    alreadyInVariants = 'YES';
                    variantsIndex = i;
                }
            }

            if (alreadyInVariants == 'YES') {
                $scope.variantsArray.splice(variantsIndex, 1);
				alert('3');
				alert(JSON.stringify($scope.variantsArray))
                return;
            } 
			else {
                $scope.variantsArray.push(variants);
				alert(JSON.stringify($scope.variantsArray));
                return;
            }
		}

		
		
		
		
		
	//FUNCTION FOR ADD PRODUCT VARIANTS TO CART============================================================================ 
        _this.addVariant = function() {
            // alert(JSON.stringify($scope.product)); 
            //$scope.value =  $('#v').val();
            //alert($scope.value);
            if ($scope.variantsArray == '') {
                alert('Please choose atleast one variant');
            } 
		    else {
			    $scope.item.variants = $scope.variantsArray;
                $scope.item.quantity = 1;
                var price = $scope.item.item_price;
                for (var j = 0; j < $scope.variantsArray.length; j++) {
                    if ($scope.variantsArray[j].price) {
                        price = $scope.variantsArray[j].price;
                    }
                }

                var productToAdd = angular.copy($scope.item);
                productToAdd.item_price = price;
                for (var j = 0; j < $scope.variantsArray.length; j++) {
                    if ($scope.variantsArray[j].price_difference) {
                        productToAdd.item_price = parseInt(productToAdd.item_price) + parseInt($scope.variantsArray[j].price_difference);
                    }
                }
                productToAdd.discounted_price = productToAdd.item_price  ;
         
                for (var j = 0; j < $scope.variantsArray.length; j++) {
                    if ($scope.item.id == $scope.variantsArray[j].product_id) {
                        $scope.variantsSelectedTitle.push($scope.variantsArray[j].title);
                    }
                    $scope.variantsSelectedTitleData = $scope.variantsSelectedTitle.toString();
                }
			
			        
		   
                _this.cartStorage.items.push(productToAdd);
                $("#variantsModal").modal('hide');
			
			    
            }
        }
	
		 
	//========= FUNCTIONS FOR GET DATA SEARCH LIST VIA API===============================================================================================
    //===========================================================================================================================================
        _this.querySearch = function(query, field) {  
            return $http.get(APP_URL + "/" + field.api, {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
                return response.data.data.data;
            })
        };

	 
	
        _this.selectedValueChange = function(data,field ) { 
            $log.info('Data changed to ' + JSON.stringify(data));
            if (field.identifier == 'customer_id') {
                 field.value = data.value; 
            } 
            $scope.selected_values = '';
        }
		
		
	  
	  
	  
	  
	  		   _this.proceed = function( ) {  
			   
localStorage.setItem("sample_data", JSON.stringify(_this.cartStorage.items));

sessionStorage.setItem("sample_data1", JSON.stringify(_this.cartStorage.items));

localStorage.setItem("env", 'dev');


console.log(localStorage.getItem('sample_data'));
console.log(sessionStorage.getItem('sample_data1'));

if(localStorage.getItem('sample_data') != null){
	alert('1');
   $scope.data = JSON.parse(localStorage.getItem('sample_data'));
   console.log($scope.data);
   $scope.env = localStorage.getItem("env");
   console.log($scope.env);
}


			   $window.location.href = 'checkout'

	}
	
	

    }]);



//==========================================================CART CONTROLLER==========================================================================================================================
//========================================================================================================================================================================================================
    app.controller('cartController', function($http, cartStorage,$scope,$window, $log, $q, $timeout, toastr) {
        var _this = this;
        _this.cartStorage = cartStorage.cart;

	    //FUNCTION FOR INCREMENT item QUANTITY============================================================
        _this.increaseItemAmount = function(item) { 
            item.price = item.base_price;
            var quantity = item.quantity++; 
		    
			  $scope.order_products = _this.cartStorage.items;
			$scope.order_products.data = {"coupon_code":"Testd1",  "store_id":"11", "loyalty_points":"","customer_id":"266","items": $scope.order_products};
				 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_products.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
                $(".cart").show();
                $(".main-cart").hide();					 
				console.log(JSON.stringify(_this.paymentData));
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
			
			document.getElementById('quantity').value = quantity;
            item.totalPrice = item.price * item.quantity;
		
        }

		
		//FUNCTION FOR DECREMENT PRODUCT QUANTITY IN CART============================================================
        _this.decreaseItemAmount = function(item) {
            item.quantity--;
		    $scope.order_products = _this.cartStorage.items;
			$scope.order_products.data = {"coupon_code":"Testd1",  "store_id":"11", "loyalty_points":"","customer_id":"266","items": $scope.order_products};
				 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_products.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
                $(".cart").show();
                $(".main-cart").hide();					 
				console.log(JSON.stringify(_this.paymentData));
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
				
            if (item.quantity <= 0) {
			    item.quantity = 0;
                item.addedToCart = false;
                item.showAddToCart = false;
                var itemIndex = _this.cartStorage.items.indexOf(item);
                
				if (itemIndex > -1) {
				    _this.cartStorage.items.splice(itemIndex, 1);
                    document.getElementById("res8").value = JSON.stringify(_this.cartStorage.items);
                    _this.enableMe = true;
				}
            }
        }

		
		
		//FUNCTION FOR REMOVE PRODUCT FROM CART============================================================
        _this.removeFromCart = function(product) {
            var itemIndex = _this.cartStorage.items.indexOf(product);
            _this.cartStorage.items.splice(itemIndex, 1); 
		   
		     $scope.order_products = _this.cartStorage.items;
			$scope.order_products.data = {"coupon_code":"Testd1",  "store_id":"11", "loyalty_points":"","customer_id":"266","items": $scope.order_products};
				 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_products.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
                $(".cart").show();
                $(".main-cart").hide();					 
				console.log(JSON.stringify(_this.paymentData));
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });

            /*	   product.quantity = 0;
            product.addedToCart = false;
            product.showAddToCart = false;
            var itemIndex = _this.cartStorage.products.indexOf(product);
			alert(itemIndex);
            if (itemIndex > -1) {
                var a =  _this.cartStorage.products.splice(itemIndex, 1);
				alert(JSON.stringify(a));
				alert(JSON.stringify(_this.cartStorage.products)); */
                document.getElementById('res1').value = JSON.stringify(_this.cartStorage.products);
            //$scope.enableMe = true;
            // }
        }
		
		 _this.$watch('shopData',function(){
      cartStorage.setItem(_this.cartStorage);
    })
		
    });

	
	 
