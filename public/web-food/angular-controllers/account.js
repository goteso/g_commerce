  
app.controller('accountController', function($http,$scope) { 


$scope.user_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1); 


  var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user/'+$scope.user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.editUser  = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  

		
 
		
		 //==========================function for update user======================================
		   $scope.updateUser = function(){
			 
			    var photo = $('#item_photo').val();
				 
				 for(var i=0;i<$scope.editUser[0].fields.length;i++){
				 if($scope.editUser[0].fields[i].identifier == 'photo'){
					 $scope.editUser[0].fields[i].value = photo; 
				 } 
				}
				
				console.log(JSON.stringify($scope.editUser));
				
				
			    var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/form/user/'+$scope.user_id,
            data:  $scope.editUser,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data; 

           /* if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); }
             */
            

            $('#loading').css('display', 'none');    
			 console.log(JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {  	
		
            // toastr.error(data.message, 'Error');
		      console.log(JSON.stringify(data));
        });  
 
		   };			   
 
});


 