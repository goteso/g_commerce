 
 app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/web-index.html",
        controller : "indexController"
    })
    .when("/stores", {
        templateUrl : APP_URL+"/web-stores.html",
        controller : "storesController"
    })
    .when("/items", {
        templateUrl : APP_URL+"/cart-items.html",
        controller : "orderController as main"
    })
    .when("/checkout", {
        templateUrl : APP_URL+"/checkout.html",
        controller : "checkoutController as main"
    })
});
		
		
		
		 

		
app.factory('storeData', function() {
			 var sData = {}
			 function set(data){
				 sData = data;
			 }
			 function get(){
				 return sData;
			 }
			 return{
				 set:set,
				 get:get
			 }
		});
		
		
		
		app.controller('indexController', function ($http, $scope, $window,toastr,$log,storeData,$location) {
			
			//====================FUNCTION FOR GET FEATURED STORES IMAGES ==========================================================================
			
			 var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores?fetured=1&per_page=6',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.featuredRest  = data.data.data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
        });  
		
		
		
				//====================FUNCTION FOR GET AREA API==========================================================================
		$scope.areaSearch = function(query) { 
            return $http.get(APP_URL + "/api/v1/areas?search_text=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
			console.log(JSON.stringify(response.data.data.data));
                return response.data.data.data;
				
            })
        };
  
        $scope.selectedAreaChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
                 $scope.area_id = data.area_id;  
				 $scope.area_lat = data.latitude; 
				 $scope.area_lon = data.longitude;  
        } 
		
		 $scope.searchAreaChange = function(text){  
				   $scope.area_id = ''; 
			};
		
		
		
		
		//====================FUNCTION FOR GET CUSINIES API==========================================================================
		$scope.cuisinesSearch = function(query) { 
            return $http.get(APP_URL + "/api/v1/store-filter-options?type=cuisines&search_text=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
			console.log(JSON.stringify(response.data.data.data));
                return response.data.data.data;
				
            })
        };
  
        $scope.selectedCuisinesChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
                 $scope.cuisines =   data.value;  
        } 
		
		 $scope.searchCuisinesChange = function(text){  
				   $scope.cuisines = ''; 
			};
		
		
		$scope.catering_status = 0;
		
		$scope.getItemType = function(value){
			if(value == 'catering'){
				$scope.catering_status = 1;
			} 
			else{
				$scope.catering_status = 0;
			}
			
			
		}
		
		
		
		
		
		$scope.searchStore = function () {
			console.log(APP_URL+'/api/v1/stores?page=1&orderby=distance&latitude='+$scope.area_lat+'&longitude='+$scope.area_lon+'&exclude_busy_stores=false&catering_status='+$scope.catering_status+'meta_filters=cuisines:'+$scope.cuisines);
			 var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores?page=1&orderby=distance&latitude='+$scope.area_lat+'&longitude='+$scope.area_lon+'&exclude_busy_stores=false&catering_status='+$scope.catering_status+'meta_filters=cuisines:'+$scope.cuisines,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.storeData  = data.data; 
      
	   
	   storeData.set($scope.storeData); 
	   $location.path("/stores");
    
	
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
        });  
		
		};
	
	
		
		
		});
		
 //====================================================== REVIEW CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('storesController', function ($http, $scope, $window,toastr, storeData,$location) {
 
	     // get basic form to add product ============================================================== clean done
	      
         $('#loading').css('display', 'block');
		  
		 $scope.storesList = storeData.get();

		  $('#loading').css('display', 'none');
		 console.log(JSON.stringify($scope.storesList));

     
		
		 //================================ FUNCTION FOR GET CUSINIES FILTER LIST  ================================================================================
	 
    	   
		        var request = $http({
                    method: "GET",
                    url: APP_URL+'/api/v1/store-filter-options?type=cuisines',
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.cusiniesList = data;
					 				
			    })
				.error(function (data, status, headers, config) { 
			        console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		   
		   
		   $scope.cuisinesFiler = function(value){
			    alert(value);
			   return;
			    var request = $http({
                    method: "GET",
                    url: APP_URL+'/api/v1/stores?type=cuisines:'+value,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.storesList = data;
					 				
			    })
				.error(function (data, status, headers, config) { 
			        console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		
		
		   }
		   
		   $scope.ratingSort = function(){
		
		
		   }
		   
		   $scope.minOrderSort = function(){
		
		
		   }
	
		
	});
		
		
		
		
		
		
		 
app.factory('cartStorage', function() {
    var _cart = {
        items: [], 
    };
    var service = {
        get cart() {
            return _cart;
        }
    }
    return service;
});




		
		//==========================================================ADD ORDER CONTROLLER====================================================================================================
//==================================================================================================================================================================================
    app.controller('orderController', function($http,$scope,$window,$log, toastr, cartStorage,$location) {
     
	$scope.store_id = window.location.href.substr(window.location.href.lastIndexOf('?') + 1);
	 
        var _this = this;
       
	      
	 
	 
	    _this.cartStorage = cartStorage.cart;
        _this.total = 0;

        $('.order-content').css('display', 'none');
        $('#loading').css('display', 'block'); 
		$(".promoCode").css('display', 'none');
        
		//========= ONLOAD GET ADD ORDER FORM  VIA CALLING API==================================================================
		
		 var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/category/?store_id='+$scope.store_id+'&include_items=true',
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
           //console.log(JSON.stringify(data)); 
			 _this.addOrder = data.data.data;
			 console.log(JSON.stringify(_this.addOrder));  
            $('#loading').css('display', 'none');
            $('.order-content').css('display', 'block');
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
		
		
		
		//========= ONLOAD GET ADD ORDER FORM  VIA CALLING API==================================================================
		/* var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/orders/add/form/',
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. *
        request.success(function (data) { 
           //console.log(JSON.stringify(data)); 
			 _this.addOrder = data.categories_products;
            _this.addOrderMeta = data.order_meta_fields;
            _this.UserListData = data.order_meta_fields.fields.fields;
            $('#loading').css('display', 'none');
            $('.order-content').css('display', 'block');
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
		
		*/
		 
	  _this.orderCalculate = function() { 
			  $scope.order_items = _this.cartStorage.items;
			$scope.order_items.data = {"coupon_code":$scope.coupon_code,  "store_id":$scope.store_id, "loyalty_points":$scope.loyalty_points,"customer_id":"266","items": $scope.order_items};
				 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_items.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
				_this.points_title = _this.paymentData.points_title;
                $(".cart").hide();
                $(".main-cart").show();	
                $(".promoCode").show();				
				console.log(JSON.stringify(_this.paymentData));
				 _this.cartStorage.payment = _this.paymentData;
				
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
			
		 
		}; 
		
		
		
		
		
		
		
		//========= FUNCTIONS FOR APPLY COUPON ====================================================================================
	//=================================================================================================================================
		
		 _this.applyCoupon = function() {
		
		_this.pointsDisabled = true;
		 $scope.coupon_code = $('#coupon_code').val(); 
		  $scope.order_items = _this.cartStorage.items;
			_this.order_items.data = {"coupon_code":$scope.coupon_code,  "store_id":$scope.store_id, "loyalty_points":$scope.loyalty_points,"customer_id":"266","items": $scope.order_items};
				 
				 _this.cartStorage.coupons = _this.order_items.data;
				 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_items.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
				_this.cartStorage.payment = _this.paymentData;
                $(".cart").hide();
                $(".main-cart").show();					 
				console.log(JSON.stringify(_this.paymentData)); 
				_this.paymentData.couponMessage =  _this.paymentData.data[0].discount_response.message; 
				
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
		
		 }
		 
		 
		 
		 //========= FUNCTIONS FOR APPLY Points ====================================================================================
	//=================================================================================================================================
		
		 _this.changePonits = function() { 
		 if (_this.points == true){
		 $scope.loyalty_points = 1;}
else{
$scope.loyalty_points = 0;
} 
		 alert($scope.loyalty_points);
		  $scope.order_items = _this.cartStorage.items;
			_this.order_items.data = {"coupon_code":$scope.coupon_code,  "store_id":$scope.store_id, "loyalty_points":$scope.loyalty_points,"customer_id":"266","items": $scope.order_items};
				  _this.cartStorage.coupons = _this.order_items.data;
				  
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_items.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
				_this.cartStorage.payment = _this.paymentData;
                $(".cart").hide();
                $(".main-cart").show();					 
				console.log(JSON.stringify(_this.paymentData)); 
				_this.paymentData.couponMessage =  _this.paymentData.data[0].discount_response.message; 
				
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
		
		 }
		 
		 
		 
		 
		 

	//========= FUNCTIONS FOR ADD PRODUCTS TO CART ====================================================================================
	//=================================================================================================================================
	
	
        _this.addToCart = function(item) {
            $scope.item = item;
            $scope.id = item.item_id;
            $scope.item_id = {"item_id": $scope.id }
			
			//CHECK VARIANTS EXIST OR NOT=====================================================================
			
			//IF DOES NOT EXIT-----------------------------------------------------------------
            if (item.variant_exist == '0') {
                var alreadyInCart = '';
                var cartIndex = 0;

                if (_this.cartStorage.items.length == 0) {
                    item.discounted_price = item.item_price;
                    item.quantity = 1;
				    item.variants = [];
                    _this.cartStorage.items.push(item); 
				 _this.orderCalculate();
                    return;
				}

                for (var i = 0; i < _this.cartStorage.items.length; i++) {
                    if (_this.cartStorage.items[i].item_id == item.item_id) {
                        alreadyInCart = 'YES';
                        cartIndex = i;
                    }
                }

				
                if (alreadyInCart == 'YES') {
                    _this.cartStorage.items[cartIndex].quantity = _this.cartStorage.items[cartIndex].quantity + 1;
				 _this.orderCalculate();
                    return;
                } 
				
				else {
                    item.quantity = 1;
				    item.variants = [];
                    item.discounted_price = item.item_price;
                    _this.cartStorage.items.push(item); 
					
					 _this.orderCalculate();
                    document.getElementById("res").value = JSON.stringify(_this.cartStorage.items);
				
				   
                }
            }
 
			//IF VARIANTS EXIT------------------------------------------------------------------
			else { 
			
				//CALL API FOR GET PRODUCT VARIANTS===========================
				var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/items-variants?orderby=item_variant_value_title&item='+$scope.id,
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			 _this.variantsdata = data.data;
            console.log(JSON.stringify(data)); 
            //$('#loading').css('display', 'none');
			 $("#variantsModal").modal('show');
                     $scope.variantsArray = [];
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
			
				 
            }

        };

		
		  //$scope.variantsSelectedTitle = [];
		
		
		//FUNCTION FOR SHOW PRODUCT VARIANTS AS CHECKBOXES=======================================================================
       _this.getVariantValue = function(variants) { 
            $scope.variantsSelectedTitle = []; 
            var alreadyInVariants = '';
            var variantsIndex = 0;
            $scope.variant = variants;
            if ($scope.variantsArray.length == 0) { 
				
                $scope.variantsArray.push(variants); 
                return;
            }

            for (var i = 0; i < $scope.variantsArray.length; i++) {
                if ($scope.variantsArray[i].item_variant_value_id == variants.item_variant_value_id) { 
                    alreadyInVariants = 'YES';
                    variantsIndex = i;
                }
            }

            if (alreadyInVariants == 'YES') {
                $scope.variantsArray.splice(variantsIndex, 1); 
                return;
            } 
			else {
                $scope.variantsArray.push(variants); 
                return;
            }
		}

		
		
		 //FUNCTION FOR SHOW PRODUCT VARIANTS AS RADIO BUTTONS=====================================================================
        _this.getVariantValue2 = function(variants) {
			alert(JSON.stringify(variants));
		    $scope.variantsSelectedTitle = [];
            //alert(JSON.stringify(variants));
            var alreadyInVariants = '';
            var variantsIndex = 0;

            if ($scope.variantsArray.length == 0) {
               $scope.variantsArray.push(variants); 
                return;
            }

            for (var i = 0; i < $scope.variantsArray.length; i++) {
                if ($scope.variantsArray[i].product_variant_type_id == variants.product_variant_type_id) {
                   alreadyInVariants = 'YES';
                   variantsIndex = i;
                }
            }

            if(alreadyInVariants == 'YES') {
                $scope.variantsArray.splice(variantsIndex, 1); 
                $scope.variantsArray.push(variants);  
                return;
            } 
			else {
                $scope.variantsArray.push(variants); 
                return;
            }
		}

        /* if (itemIndex > -1) {
                        _this.cartStorage.products.splice(itemIndex, 1);
    					_this.enableMe = true;
             }
    	 */

		 
		 

    $scope.variantsSelectedTitle = [];
		
		
	//FUNCTION FOR ADD PRODUCT VARIANTS TO CART============================================================================ 
        _this.addVariant = function() {
            // alert(JSON.stringify($scope.product)); 
            //$scope.value =  $('#v').val();
            //alert($scope.value);
            if ($scope.variantsArray == '') {
                alert('Please choose atleast one variant');
            } 
		    else {
			    $scope.item.variants = $scope.variantsArray;
                $scope.item.quantity = 1;
                var price = $scope.item.item_price;
                for (var j = 0; j < $scope.variantsArray.length; j++) {
                    if ($scope.variantsArray[j].price) {
                        price = $scope.variantsArray[j].price;
                    }
                }

                var productToAdd = angular.copy($scope.item);
                productToAdd.item_price = price;
                for (var j = 0; j < $scope.variantsArray.length; j++) {
                    if ($scope.variantsArray[j].price_difference) {
                        productToAdd.item_price = parseInt(productToAdd.item_price) + parseInt($scope.variantsArray[j].price_difference);
                    }
                }
                productToAdd.discounted_price = productToAdd.item_price  ;
         
                for (var j = 0; j < $scope.variantsArray.length; j++) {
                    if ($scope.item.id == $scope.variantsArray[j].item_id) {
                        $scope.variantsSelectedTitle.push($scope.variantsArray[j].item_variant_value_title);
                    }
                    $scope.variantsSelectedTitleData = $scope.variantsSelectedTitle.toString();
                }
			
			        
		   
                _this.cartStorage.items.push(productToAdd);
				_this.orderCalculate();
                $("#variantsModal").modal('hide');
			
			    
            }
        }
	
		 
	//========= FUNCTIONS FOR GET DATA SEARCH LIST VIA API===============================================================================================
    //===========================================================================================================================================
        _this.querySearch = function(query, field) {  
            return $http.get(APP_URL + "/" + field.api, {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
                return response.data.data.data;
            })
        };

	 
	
        _this.selectedValueChange = function(data,field ) { 
            $log.info('Data changed to ' + JSON.stringify(data));
            if (field.identifier == 'customer_id') {
                 field.value = data.value; 
            } 
            $scope.selected_values = '';
        }
		
		
	  
	  
	  
	  
	  		   _this.proceed = function( ) {  
			   
localStorage.setItem("sample_data", JSON.stringify(_this.cartStorage.items));

sessionStorage.setItem("sample_data1", JSON.stringify(_this.cartStorage.items));

localStorage.setItem("env", 'dev');


console.log(localStorage.getItem('sample_data'));
console.log(sessionStorage.getItem('sample_data1'));

if(localStorage.getItem('sample_data') != null){
	alert('1');
   $scope.data = JSON.parse(localStorage.getItem('sample_data'));
   console.log($scope.data);
   $scope.env = localStorage.getItem("env");
   console.log($scope.env);
}


			   $window.location.href = 'checkout'

	}
	
	

    });



//==========================================================CART CONTROLLER==========================================================================================================================
//========================================================================================================================================================================================================
    app.controller('cartController', function($http, cartStorage,$scope,$window, $log, $q, $timeout, toastr) {
        var _this = this;
        _this.cartStorage = cartStorage.cart;

	    //FUNCTION FOR INCREMENT item QUANTITY============================================================
        _this.increaseItemAmount = function(item) { 
            item.price = item.base_price;
            var quantity = item.quantity++; 
		    
			  $scope.order_products = _this.cartStorage.items;
			$scope.order_products.data = {"coupon_code":"Testd1",  "store_id":"11", "loyalty_points":"","customer_id":"266","items": $scope.order_products};
				 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_products.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
                $(".cart").show();
                $(".main-cart").hide();					 
				console.log(JSON.stringify(_this.paymentData));
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
			
			document.getElementById('quantity').value = quantity;
            item.totalPrice = item.price * item.quantity;
		
        }

		
		//FUNCTION FOR DECREMENT PRODUCT QUANTITY IN CART============================================================
        _this.decreaseItemAmount = function(item) {
            item.quantity--;
		    $scope.order_products = _this.cartStorage.items;
			$scope.order_products.data = {"coupon_code":"Testd1",  "store_id":"11", "loyalty_points":"","customer_id":"266","items": $scope.order_products};
				 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_products.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
                $(".cart").show();
                $(".main-cart").hide();					 
				console.log(JSON.stringify(_this.paymentData));
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
				
            if (item.quantity <= 0) {
			    item.quantity = 0;
                item.addedToCart = false;
                item.showAddToCart = false;
                var itemIndex = _this.cartStorage.items.indexOf(item);
                
				if (itemIndex > -1) {
				    _this.cartStorage.items.splice(itemIndex, 1);
                    document.getElementById("res8").value = JSON.stringify(_this.cartStorage.items);
                    _this.enableMe = true;
				}
            }
        }

		
		
		//FUNCTION FOR REMOVE PRODUCT FROM CART============================================================
        _this.removeFromCart = function(product) {
            var itemIndex = _this.cartStorage.items.indexOf(product);
            _this.cartStorage.items.splice(itemIndex, 1); 
		   
		     $scope.order_products = _this.cartStorage.items;
			$scope.order_products.data = {"coupon_code":"Testd1",  "store_id":"11", "loyalty_points":"","customer_id":"266","items": $scope.order_products};
				 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_products.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
                $(".cart").show();
                $(".main-cart").hide();					 
				console.log(JSON.stringify(_this.paymentData));
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });

            /*	   product.quantity = 0;
            product.addedToCart = false;
            product.showAddToCart = false;
            var itemIndex = _this.cartStorage.products.indexOf(product);
			alert(itemIndex);
            if (itemIndex > -1) {
                var a =  _this.cartStorage.products.splice(itemIndex, 1);
				alert(JSON.stringify(a));
				alert(JSON.stringify(_this.cartStorage.products)); */
                document.getElementById('res1').value = JSON.stringify(_this.cartStorage.products);
            //$scope.enableMe = true;
            // }
        }
		
	
		
    });

	
	 
	  app.controller('checkoutController', function($http,$scope,$window,$log, toastr, cartStorage,$location,) {
     
	  var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/orders/add/form/',
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
           //console.log(JSON.stringify(data)); 
			 _this.addOrder = data.categories_products;
            _this.addOrderMeta = data.order_meta_fields;
            _this.UserListData = data.order_meta_fields.fields.fields;
            $('#loading').css('display', 'none');
            $('.order-content').css('display', 'block');
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
		
		
	
        var _this = this; 
	     _this.cartStorage = cartStorage.cart;
		console.log(JSON.stringify(_this.cartStorage));
		
		
		
		//========= FUNCTIONS FOR SELECT ADDRESS ==========================================================================================
	//=================================================================================================================================
         
		//FUNCTION FOR GET ADDRESSES======================================================================
		$scope.get_address = function(parent_identifier) {
             alert(parent_identifier);
            var customer_id = $('#' + parent_identifier).val();
			alert(customer_id);
            if (customer_id == '') {
                alert('Please Select User First');
            } 
			else {
                $("#myModal").modal('show');

                /*get data from api**/
                $http.get(APP_URL + '/api/v1/address/?linked_id=2')
                .success(function(data, status, headers, config) {
                    _this.data_addresses = data.data;
                    _this.currentpage_addresses = data.current_page;
                    _this.user_id = user_id;
                })
                .error(function(data, status, headers, config) {
                    document.getElementById('res').value = JSON.stringify(data);
                });
            }
        };
		 
	    //FUNCTION FOR SELECT ADDREES VALUE INDEX=========================================================
        $scope.checkedIndex = function(values) { 
            $scope.selected_values = values; 
            //alert(JSON.stringify($scope.selected_values));
        }
 
    
	    //FUNCTION FOR PUT SELECTED ADDREES VALUE IN INPUT FIELD=========================================
        $scope.isSelected = function(field) {
            if (field.identifier == 'delivery_address') {
               //alert(JSON.stringify(field.id));
               field.value = JSON.stringify($scope.selected_values);
            }
           // return $scope.selected_values === values;
        }

	
 $scope.selected = 0;
	
	//========= FUNCTIONS FOR SELECT PICKUP & DELIVERY DATE-TIME ================================================================================
	//============================================================================================================================================
	     
		//FUNCTION FOR GET PICKUP TIMESLOTS============================================================
        $scope.get_pickupTime = function() {
            $("#pickupModal").modal('show');

            $http.get(APP_URL + '/api/v1/timeslots_settings')
            .success(function(data, status, headers, config) {
                _this.pickupDateData = data.data;
                $scope.selectedValue = _this.pickupDateData[0].day;
                $scope.pickupDate2 = $scope.selectedValue;
                document.getElementById('res9').value = JSON.stringify(data);
            })
            .error(function(data, status, headers, config) {
                document.getElementById('res').value = JSON.stringify(data);
            });
        };


		//FUNCTION FOR GET SELECTED PICKUP DATE============================================================
        $scope.getPickupD = function(value) {
            $scope.pickupDate = value;
			alert($scope.pickupDate);
        }

		//FUNCTION FOR SAVE SELECTED PICKUP DATE & TIME============================================================
        $scope.savePickup = function( id, from_time, to_time) {

            if (!$scope.pickupDate) {
                $scope.pickupDate = $scope.pickupDate2;
                var time = id;
                $scope.timeShowFrom = from_time;
                $scope.timeShowTo = to_time;
                $scope.pickupTime = $scope.pickupDate3;
                $scope.pickupTimeShow = $scope.pickupDate + ' | ' + $scope.timeShowFrom + ' - ' + $scope.timeShowTo;
               for(var i=0;i<=_this.addOrderMeta.fields.length;i++){
					
					alert(_this.addOrderMeta.fields[i]);
                field.value = $scope.pickupTime;
				
				
                $("#pickupModal").modal('hide');
                $scope.deliveryTime = '';
                $scope.deliveryTimeShow = '';
				}
                document.getElementById('delivery_time').value = '';
                document.getElementById(identifier).value = $scope.pickupTime;
            } 
			else {
                var time = id;
                $scope.timeShowFrom = from_time;
                $scope.timeShowTo = to_time;
                $scope.pickupTime = $scope.pickupDate1;
                $scope.pickupTimeShow = $scope.pickupDate + ' | ' + $scope.timeShowFrom + ' - ' + $scope.timeShowTo;
				
				for(var i=0;i<=_this.addOrderMeta.fields.length;i++){
					
					alert(_this.addOrderMeta.fields[i].identifier);
                field.value = $scope.pickupTime;
				
				
                $("#pickupModal").modal('hide');
                $scope.deliveryTime = '';
                $scope.deliveryTimeShow = '';
				}
                document.getElementById('delivery_time').value = '';
                document.getElementById(identifier).value = $scope.pickupTime;
            }
        };


		
		//========= FUNCTIONS FOR PLACE ORDER ========================================================================================================
    //============================================================================================================================================
        $scope.placeOrder = function() {
            

            //$scope.msg = 'Data sent: ' + angular.toJson(_this.addOrderMeta);
            //$scope.msg1 = 'Data sent: ' + angular.toJson(_this.cartStorage.products);

           /*for (var j = 0; j < _this.addOrderMeta.fields.length; j++) {
                for (var k = 0; k < _this.addOrderMeta.fields[j].length; k++) {
                    $scope.required = _this.addOrderMeta.fields[j][k].required_or_not;
                    if ($scope.required == 1 || $scope.required == '1') {
                        $scope.valuecheck = _this.addOrderMeta.fields[j][k].value;
                        if ($scope.valuecheck == '' || $scope.valuecheck == 'undefined') {
                            toastr.error('Please ' + _this.addOrderMeta.fields[j][k].title, 'Error!');
                            return false;
                        }
                    }
                }
            }*/

            
            $scope.coupon_code = "abc";
            //$scope.total = _this.subTotal - $scope.coupon_discount;
           console.log(JSON.stringify(_this.addOrderMeta));
            //alert(JSON.stringify($scope.total));
 

            $scope.orderData = {
				 "customer_id":"1",
                 "store_id":"2",
               "coupon_code":  $scope.coupon_code, 
               "items": _this.cartStorage.items,
               "order_meta": { "fields" :_this.addOrderMeta.fields}
            };
console.log(JSON.stringify( $scope.orderData));
			//return;
  
            var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders',
                data: $scope.orderData,
                headers: {
                   'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                toastr.success('Order Added Successfully', 'Success');
                $window.location.href = 'order_detail_page?order_id=' + data.order_id;
                document.getElementById("res").value = JSON.stringify(data);
            })
			.error(function(data, status, headers, config) {
                toastr.error('Error Occurs', 'Error!');
                document.getElementById("res8").value = JSON.stringify(data);
            });

        };


	
	
		
	  });

		 