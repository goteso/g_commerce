<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use App\Traits\one_signal; // <-- you'll need this line...
use Hash;
use Mail;
use File;
 
 


class UploadImagesController extends Controller 
{
	
 
	
	//Route-25.1 ===================================================
    public function upload_image(Request $request)
    {   
	                     //for file upload
                        $path = public_path().'/users_images/';
                        if(!File::exists($path)) 
                        {
                        File::makeDirectory($path, $mode = 0777, true, true);
                        }
						
					             if(isset($request->image) && !empty($request->image))
                        {
                          $unique_string = 'profile-image-'.strtotime(date('Y-m-d h:i:s'));    
                          $file = $request->image;                       
                          $photo_name = $unique_string.$file->getClientOriginalName();
                          $ext = pathinfo($photo_name, PATHINFO_EXTENSION);
                          $thumb_name = "thumb-". $photo_name;                     
                          $file->move($path,$photo_name);
                          $this->make_thumb($path.'/'.$photo_name,$path.'/'.$thumb_name,'700' , $ext);


                       }
					            else
					             {
						             $data['status_code']  =   0;
                         $data['status_text']    =   'Failed';           
                         $data['message']        =   'File Required';
                         return $data;
                        }
					   
					             $data['status_code']  =   1;
                       $data['status_text']    =   'Success';           
                       $data['message']        =   'Image is Uploaded.';
                       $data['data'][]["image_url"] =  $photo_name;
		                   return $data;
    }
	
	
 
 public function make_thumb($src, $dest, $desired_width , $ext) 
   {

 
    /* read the source image */
  if($ext == 'webp')
  {
    $source_image = @imagecreatefromwebp(@$src);
    $width = @imagesx($source_image);
    $height = @imagesy($source_image);
  }

    if($ext == 'jpg' || $ext == 'jpeg')
  {
    $source_image = @imagecreatefromjpeg(@$src);
    $width = @imagesx($source_image);
    $height = @imagesy($source_image);
  }

    if($ext == 'png')
  {
    $source_image = @imagecreatefrompng(@$src);
    $width = @imagesx($source_image);
    $height = @imagesy($source_image);
  }

  
 

   /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }
 
 
 


}