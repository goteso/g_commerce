<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
        protected $fillable = [ 'address_type', 'linked_id' , 'address_id' , 'address_title' , 'address_phone' , 'address_line1' , 'address_line2' , 'latitude' , 'longitude' , 'city' ,'state' , 'pincode' , 'country'];
		protected $table = 'address';
		
		
		protected $casts = [ 'address_id'=>'int' ];



		
        
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }

 public function getCreatedAtFormatted2Attribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->format('M d, Y');
    }
	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
}