<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingTransactionExtra extends Model
{
        protected $fillable = [  'setting_transaction_extra_title', 'type' , 'tax_applicable' ];
		protected $table = 'setting_transaction_extra';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}