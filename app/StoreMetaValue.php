<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreMetaValue extends Model
{
    protected $fillable = [ 'store_meta_value_id' , 'store_meta_type_id' , 'store_id', 'value' ];
	protected $table = 'store_meta_value';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) 
     {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
     }
	
	

        public function getStoreMetaTypeTitleAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
        @$store_meta_type_id = @$this->store_meta_type_id;
 
        if($store_meta_type_id != 'null' && $store_meta_type_id != '' &&  $store_meta_type_id != '0' &&  $store_meta_type_id != 0)
        {
            $store_meta_type_title = @\App\StoreMetaType::where('store_meta_type_id',$store_meta_type_id)->first(['title'])->title;
        }
        else
        {
            $store_meta_type_title ='';
        }
        return $store_meta_type_title;
    }



        public function getStoreMetaTypeIdentifierAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
        @$store_meta_type_id = @$this->store_meta_type_id;
 
        if($store_meta_type_id != 'null' && $store_meta_type_id != '' &&  $store_meta_type_id != '0' &&  $store_meta_type_id != 0)
        {
            $identifier = @\App\StoreMetaType::where('store_meta_type_id',$store_meta_type_id)->first(['identifier'])->identifier;
        }
        else
        {
            $identifier ='';
        }
        return $identifier;
    }



	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}