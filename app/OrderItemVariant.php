<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItemVariant extends Model
{
        protected $fillable = [ 'order_item_id' , 'item_variant_id', 'item_variant_title' , 'order_item_variant_value' ];
		protected $table = 'order_item_variant';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}