<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationVariables extends Model
{
	
protected $fillable = ['variable_key', 'variable_value'];
protected $table = 'notification_variables';
}
