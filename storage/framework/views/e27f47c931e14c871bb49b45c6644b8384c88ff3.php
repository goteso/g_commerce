<?php echo $__env->make('emails.email_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>				
 
 

 						<tr>
									<td align="left" style="color: #66717d; font-size: 18px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 20px;" class="resize-text text_color">
										
						 	<!-- ======= section header ======= -->
							<p style="line-height: 25px;text-align: center;font-size: 15px;">
	        					   <?php echo $email_body;?>
                            </p>
        				</td>
					</tr>
					 



   <?php if( $notification_type == 'order_status_updated' || $notification_type == 'order_placed' || $notification_type == 'order_reviewed_by_customer' || $notification_type == 'order_reviewed_by_driver' || $notification_type == 'order_dropped_off_by_driver' || $notification_type == 'order_picked_up_by_driver' ): ?>


                <?php echo $__env->make('emails.includes.order_placed_email', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>	
   <?php endif; ?>



 <tr><td height="10" style="font-size: 10px; line-height:10px;">&nbsp;</td></tr>
					 
					
<?php echo $__env->make('emails.email_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>	