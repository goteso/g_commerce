<?php $__env->startSection('title', 'My Orders' ); ?>  
 
<?php $__env->startSection('content'); ?>


<?php $__env->startSection('header'); ?>
<?php echo $__env->make('web-food.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
  
 
        <!---------------------------------------BANNER SECTION STARTS HERE-------------------------------------------->
            <section id="banner" style="background: url('<?php echo e(URL::asset('web-food/assets/images/food-back-small.jpg')); ?>')no-repeat center;">
               <div class="container"> 
                  <div class="row" >
                      <div class="col-sm-12"  ng-controller="profileController" >
                           <md-list layout-padding class="profile-data" >
							<md-list-item class="md-3-line"   ng-repeat="users in profileData.data"  >
								<img  ng-src="<?php echo e(URL::asset('images/users')); ?>/{{users.photo}}" class="md-avatar" >
								<div class="md-list-item-text">
								  <h2 >{{users.first_name}} {{users.last_name}}</h2>
								  <h4>
									{{users.email}}
								  </h4>
								</div>
							</md-list-item>
						  </md-list>    
                      </div>
                 </div>
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
 
        <!---------------------------------------WORKS SECTION STARTS HERE-------------------------------------------->
            <section id="orders">
               <div class="container">
                  <div class="row"> 
                     <div class="col-sm-4 text-center">   
                         <div class="panel left">
						 <div class="panel-body">
						   <table  class="table">
						    <tr>
							  <td> <i class="fa fa-"></i></td>
							  <td > <a href="<?php echo e(URL::to('my-orders')); ?>">My Orders</a></td>
							</tr>
							<tr>
							  <td> <i class="fa fa-map-marker-alt"></i></td>
							  <td> <a href="<?php echo e(URL::to('addresses')); ?>">Manage Addresses</a></td>
							</tr>
							<tr class="active">
							  <td> <i class="fa fa-"></i></td>
							  <td><a href="<?php echo e(URL::to('reviews')); ?>"> My Reviews</a></td>
							</tr>
							<tr>
							  <td> <i class="fa fa-star"></i></td>
							  <td><a href="<?php echo e(URL::to('favourites')); ?>"> Favourites</a></td>
							</tr>
							<tr>
							  <td> <i class="fa fa-wrench"></i></td>
							  <td><a href="<?php echo e(URL::to('account')); ?>"> Account Settings</a></td>
							</tr>
							<tr>
							  <td> <i class="fa fa-sign-out-alt"></i></td>
							  <td>Signout</td>
							</tr>
						   </table>
						 </div>
						 </div>
                     </div> 
					 
					 
					 <div class="col-sm-8 text-center" ng-controller="reviewController" ng-cloak>   
                         <div class="panel">
						 <div class="panel-body">
						     <div id="tableToExport" class="products-table table-responsive"  >
                                          <table class="table" class="table table-striped" id="exportthis" >
                                             <thead>
                                                <tr> 
                                                   <th>ORDER</th>
                                                   <th>STORE</th>
                                                   <th>RATING</th>
                                                   <th>FEEDBACK</th>
                                                   <th>TIME</th> 
                                                </tr>
                                             </thead>
                                             <tbody >
                                                <tr  ng-repeat="values in reviewData.data.data  "> 
                                                   <td><a href="<?php echo e(URL::to('/v1/order_detail')); ?>/{{values.order_id}}" ><b>#{{values.order_id}}</b></a> </td>
                                                   <td>{{values.store_details[0].store_title}}  </td>
                                                   <td >{{values.rating}}</td>
                                                   <td>{{values.review}}</td>
                                                   <td>{{values.created_at_formatted}}</td> 
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div> 
						 </div>
						 </div>
                     </div> 
                  </div> 
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
		
	 
		  				  
      
<?php $__env->startSection('footer'); ?>
<?php echo $__env->make('web-food.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<!------>

<script type="text/javascript" src="<?php echo e(URL::asset('web-food/angular-controllers/reviews.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web-food.layout.web', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>