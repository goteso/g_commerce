<?php $__env->startSection('title', 'Home' ); ?>  
 <link rel="stylesheet" href=" <?php echo e(URL::asset('web-food/assets/css/style1.css')); ?>">
<style>

#main-banner .form-group{padding:5px 8px;}
@media(min-width:768px){#main-banner .form-inline{display:flex;}}
   .spinner {
   margin: 100px auto 0;
   width: 70px;
   text-align: center;
   }
   .spinner > div {
   width: 18px;
   height: 18px;
   background-color: #333;
   border-radius: 100%;
   display: inline-block;
   -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
   animation: sk-bouncedelay 1.4s infinite ease-in-out both;
   }
   .spinner .bounce1 {
   -webkit-animation-delay: -0.32s;
   animation-delay: -0.32s;
   }
   .spinner .bounce2 {
   -webkit-animation-delay: -0.16s;
   animation-delay: -0.16s;
   }
   @-webkit-keyframes sk-bouncedelay {
   0%, 80%, 100% { -webkit-transform: scale(0) }
   40% { -webkit-transform: scale(1.0) }
   }
   @keyframes  sk-bouncedelay {
   0%, 80%, 100% { 
   -webkit-transform: scale(0);
   transform: scale(0);
   } 40% { 
   -webkit-transform: scale(1.0);
   transform: scale(1.0);
   }
   }
   .foodbakery-button-loader {
   position: absolute;
   top: 0;
   left: 0;
   width: 100%;
   height: 100%;
   background: #f97456;
   text-align: center;
   border-radius: 3px;
   background-color: #c33332 !important;
   }
   .foodbakery-button-loader .spinner {
   width: 25px;
   height: 25px;
   position: absolute;
   display: inline-block;
   top: 0;
   bottom: 0;
   left: 0;
   right: 0;
   margin: auto;
   }
   .foodbakery-button-loader .double-bounce1, .foodbakery-button-loader .double-bounce2 {
   width: 100%;
   height: 100%;
   border-radius: 50%;
   background-color: #fff;
   opacity: 0.6;
   position: absolute;
   top: 0;
   left: 0;
   -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
   animation: sk-bounce 2.0s infinite ease-in-out;
   }
   .foodbakery-button-loader .double-bounce2 {
   -webkit-animation-delay: -1.0s;
   animation-delay: -1.0s;
   }
   
   md-tabs:not(.md-no-tab-content):not(.md-dynamic-height){min-height:650px!important;}
   
   .promoCode  md-tabs:not(.md-no-tab-content):not(.md-dynamic-height){min-height:130px!important;max-height:150px!important;}
   </style>
<?php $__env->startSection('content'); ?>


<?php $__env->startSection('header'); ?>
<?php echo $__env->make('web-food.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
 
  <div ng-view></div>
 
       
      
<?php $__env->startSection('footer'); ?>
<?php echo $__env->make('web-food.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<!------>


<script type="text/javascript" src="<?php echo e(URL::asset('web-food/angular-controllers/stores.js')); ?>"></script> 

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web-food.layout.web', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>