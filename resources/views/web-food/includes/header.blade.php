   <header>
   <nav class="navbar navbar-inverse  ">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home"></a>
          
        </div>
        <div id="navbar" class="navbar-collapse collapse">

          <div class="navbar-form navbar-right"> 
		  <div class="dropdown">
            <md-button type="button"  class="btn md-raised bg-color md-submit md-button md-ink-ripple  dropdown-toggle" type="button" data-toggle="dropdown">Account <span class="caret"></span></md-button>
			<ul class="dropdown-menu">
				<li>
				  <a href="{{URL::to('web-login')}}">
				    <img src="{{URL::asset('web-food/assets/images/shopping-bag.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/shopping-bag-active.svg')}}" class="img-active">
				 Login</a>
				</li>
				<li><a href="{{URL::to('register')}}">
				 <img src="{{URL::asset('web-food/assets/images/address.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/address-active.svg')}}" class="img-active">
					Register</a>
				</li>
			</div>
			
				<div class="dropdown">
			  <md-button class="btn md-raised bg-color md-submit md-button md-ink-ripple dropdown-toggle" type="button" data-toggle="dropdown">Hi, Yugal
			  <span class="caret"></span></md-button>
			  <ul class="dropdown-menu">
				<li>
				  <a href="{{URL::to('my-orders')}}">
				    <img src="{{URL::asset('web-food/assets/images/shopping-bag.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/shopping-bag-active.svg')}}" class="img-active">
				  My Orders</a>
				</li>
				<li><a href="{{URL::to('addresses')}}">
				 <img src="{{URL::asset('web-food/assets/images/address.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/address-active.svg')}}" class="img-active">
					Manage Addresses</a></li>
				<li><a href="{{URL::to('reviews')}}">
				 <img src="{{URL::asset('web-food/assets/images/feedback.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/feedback-active.svg')}}" class="img-active">
					My Reviews</a></li>
				<li><a href="{{URL::to('favourites')}}">
				 <img src="{{URL::asset('web-food/assets/images/favorites.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/favorites-active.svg')}}" class="img-active">
					Favourites</a></li>
				<li><a href="{{URL::to('account')}}">
				 <img src="{{URL::asset('web-food/assets/images/settings.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/settings-active.svg')}}" class="img-active">
					Account Settings</a></li>
				<li><a href="#">
				 <img src="{{URL::asset('web-food/assets/images/sign-out.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/sign-out-active.svg')}}" class="img-active">
					Signout</a></li>
			  </ul>
			</div>
				
          </div>
          
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
	
	</header>
	
	 
